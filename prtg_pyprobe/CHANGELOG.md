# Changelog for the Home Assistant Pugin for prtg-pyprobe
For changes regarding the prtg-pyprobe itself, check the [changelog](https://gitlab.com/paessler-labs/prtg-pyprobe/-/blob/master/CHANGELOG.md).
## 0.5.3
- Initial release of prtg-pyprobe plugin.

## 1.0.1
- Version bump to reflect pyprobe version (no changes in the plugin)

## 1.0.2
- Version bump to reflect pyprobe version (no changes in the plugin)

## 1.1.0
- Version bump to reflect pyprobe version (no changes in the plugin)

## 1.1.1
- Version bump to reflect pyprobe version (no changes in the plugin)

## 1.1.2
- Version bump to reflect pyprobe version (no changes in the plugin)

## 1.1.3
- Version bump to reflect pyprobe version (no changes in the plugin)

## 1.1.4
- Version bump to reflect pyprobe version (no changes in the plugin)