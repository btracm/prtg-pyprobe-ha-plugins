# Documentation for the Home Assistant Pugin for prtg-pyprobe
For detail on the prtg-pyprobe itself and how to use the Home Assistant Plugin, see the [docs](https://paessler-labs.gitlab.io/prtg-pyprobe/) or the [section about Home Assistant](https://paessler-labs.gitlab.io/prtg-pyprobe/examples/homeassistant/). 

## Configuration
The following values are mandatory for the configuration of the plugin. Simply copy and paste them into the Configuration field and adjust the values.
```
disable_ssl_verification: true  
log_file_location: ''
log_level: DEBUG
probe_access_key: miniprobe
probe_access_key_hashed: cd7b773e2ce4205e9f5907b157f3d26495c5b373
probe_base_interval: '60'
probe_gid: YOUR_GID
probe_name: YOUR_PROBE_NAME
probe_protocol_version: '1'
prtg_server_ip_dns: YOUR_PRTG_INSTANCE
prtg_server_port: '443'
probe_task_chunk_size: '20'
```
### Explanation of used values
**disable_ssl_verification**: Set to true if you are using a self signed certificate or you don't have a valid certificate for the PRTG installation with which you are communicating  
**log_file_location**: Leave as an empty string when running the container, the pyprobe will log to STDOUT
**log_level**: The log level of the pyprobe, possible values are "CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"  
**probe_access_key**: The access key defined in PRTG (under Setup | Core & Probes)  
**probe_access_key_hashed**: The hashed probe access key. See [Hash Access Key](https://paessler-labs.gitlab.io/prtg-pyprobe/#how-to-easily-create-a-hash-access-key) for details.  
**probe_base_interval**: The interval which the sensors are scheduled on the PRTG Core  
**probe_gid**: The unique identifier of the probe in [UUID](https://docs.python.org/3/library/uuid.html#uuid.uuid4) format. See [Generate UUID](https://paessler-labs.gitlab.io/prtg-pyprobe/#how-to-easily-generate-a-uuid) for details.  
**probe_name**: The name of the probe to be shown in the PRTG Web Interface.  
**probe_protocol_version**: Has to be *1*. This is needed for communication.  
**prtg_server_ip_dns**: IP/DNS address of your PRTG Server.  
**prtg_server_port**: The https port of PRTG's web interface (only TLS is currently supported).  
**probe_task_chunk_size**: Number of tasks which are processed in parallel.